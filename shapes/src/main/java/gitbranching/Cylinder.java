//Farhan Khandaker 2135266


package gitbranching;

public class Cylinder implements Shape3d{
    
    private double radius;
    private double height;
    
    /**
     * Constructor
     */
    public Cylinder(double radius, double height){
        this.radius = radius;
        this.height = height;
    }
    
    /**
     * Returns the volume.
     */
    @Override
    public double getVolume(){
        return Math.PI * Math.pow(this.radius, 2) * this.height;
    }
    
    /**
     * Returns the surface area.
     */
    @Override
    public double getSurfaceArea(){
        return 2 * Math.PI * Math.pow(this.radius, 2) + 2 * Math.PI * this.radius * this.height;
    }

    /**
     * Get method for Radius
     * @return
     */
    public double getRadius() {
        return radius;
    }
    
    /**
     * Get method for height
     * @return
     */
    public double getHeight() {
        return height;
    }
}
