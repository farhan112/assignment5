// Artem Babin
package gitbranching;

public class Sphere implements Shape3d {
    
    private double radius;

    /**
     * Sphere constructor
     * @param radius
     */
    public Sphere(double radius) {
        this.radius = radius;
    }

    /**
     * Returns the radius
     * @return double
     */
    public double getRadius(){
        return this.radius;
    }

    /* Calculates the volume of the sphere
     * 
     */
    @Override
    public double getVolume() {
        return Math.PI * Math.pow(this.radius, 3); 
    }

    /* Calculates the surface area of a sphere
     * 
     */
    @Override
    public double getSurfaceArea() {
        return 4 * Math.PI * Math.pow(this.radius, 2);
    }
}
