// Artem Babin
package gitbranching;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CylinderTest {

    private final double epsilon = 0.00000001;
    
    @Test
    public void cylinderTest() {
        Cylinder c1 = new Cylinder(2, 6);
        assertEquals(2, c1.getRadius(), epsilon);
        assertEquals(6, c1.getHeight(), epsilon);
    }

    @Test
    public void getVolumeTest() {
        Cylinder c1 = new Cylinder(2, 6); 
        assertEquals(Math.PI * 24, c1.getVolume(), epsilon);
    }


    @Test
    public void getSurfaceAreaTest() {
        Cylinder c1 = new Cylinder(2, 6); 
        assertEquals(32 * Math.PI, c1.getSurfaceArea(), epsilon);
    }

}
