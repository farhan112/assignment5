//Farhan Khandaker 2135266

package gitbranching;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SphereTest {
    final double tolerance = 0.00000001;

    @Test
    public void testGetMethods(){
        Sphere test = new Sphere(4);
        assertEquals(4, test.getRadius(), tolerance);
    }

    @Test
    public void testGetVolume(){
        Sphere test = new Sphere(4);
        assertEquals(Math.PI*Math.pow(4, 3), test.getVolume(), tolerance);
    }

    @Test
    public void testGetSurfaceArea(){
        Sphere test = new Sphere(4);
        assertEquals(4*Math.PI*Math.pow(4, 2), test.getSurfaceArea(), tolerance);
    }

}
